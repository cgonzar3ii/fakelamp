<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>

<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title>Lista Categorias</title>
    </head>
    <body>

        <table class="table table-striped">
            <h1>Lista Productos de la categoria <?= $id_categoria_seleccionada ?></h1>
            <tr>
                <td>ID PRODUCTO</td>
                <td>NOMBRE</td>
                <td>PRECIO EN €</td>
                <td>ID CAT.</td>
            </tr>  
            <div style="opacity:0%; position: absolute"><?= $sumaprecio = 0 ?></div>
            <?php foreach ($resultado_select_productos as $todos): ?>


                <tr>
                    <td>
                        <?= $todos->id ?>
                    </td>
                    <td>
                        <?= $todos->nombreProd ?>
                    </td>
                    <td>
                        <?= $todos->precio ?>
                    </td>
                    <td>
                        <?= $todos->categoria_id ?>
                    </td>

                </tr>

                <div style="opacity:0%; position: absolute">    
                    <?= $sumaprecio = $todos->precio + $sumaprecio ?>
                </div>


            <?php endforeach; ?>

            <tr>
                <td></td>
                <td></td>
                <td><p><?= $sumaprecio ?> (suma de los precios)</p></td>
                <td></td>
            </tr>


        </table>
        <a href="<?= site_url('/anadirproductos/' . $id_categoria_seleccionada) ?>" class="btn btn-primary ml-5">AÑADIR PRODUCTOS</a>
        <a href="<?= site_url('/listacat') ?>" class="btn btn-danger ml-2">VOLVER A CATEGORIAS</a>


    </body>
</html>
