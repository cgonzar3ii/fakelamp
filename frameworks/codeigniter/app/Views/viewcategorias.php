<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>

<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        
        <title>Lista Categorias</title>
    </head>
    <body>

        <table class="table table-striped">
<h1>Lista Categorias</h1>
                <?php foreach ($resultadoselect as $todos): ?>
                <tr>
                    <td>
                        <?= $todos->id ?>
                    </td>
                    <td>
                        <?= $todos->nombreCat ?>
                    </td>
                    <td>
                        <a href="<?= site_url("/listacat/".$todos->id)?>" class="btn btn-primary">VER PRODUCTOS</a>
                    </td>

                </tr>

            <?php endforeach; ?>
        </table>
        <a href="<?= site_url('/listaped') ?>" class="btn btn-warning ml-2">VER PEDIDOS</a>
        
    </body>
</html>
