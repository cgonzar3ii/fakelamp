<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title>Lista Pedidos</title>

    </head>
    <body>

        <table class="table table-striped">
            <br><br>
            <a href="<?= site_url('/listacat') ?>" class="btn btn-warning ml-2">VOLVER A CATEGORIAS</a> 
            <br><br>
            <form action="<?= site_url('/listaped/search') ?>" method="post">
                <label for='id_empleado_buscar' class="ml-4">
                    Filtrar pedidos por ID del empleado responsable.
                </label><br>
                <input type='number' name='id_empleado_buscar' size='6' placeholder='id emp' class='ml-4'>
                <input type="submit" value="Buscar" class="ml-1">
            </form>
            
            <br><br><br>
            
            <h1>Lista Pedidos</h1>
            
            <tr>
                <th>ID PEDIDO</th>
                <th>ID PRODUCTO</th>
                <th>PRECIO</th>
                <th>CANTIDAD</th>
                <th>                                
                    <?php if ( isset($id_empleado_buscar)): ?>
                    <p>CODIGO EMPLEADO</p>
                    <?php else: ?>
                    <p> </p>
                    <?php endif ?>
                </th>
            </tr>
            
            <?php foreach ($resultado_select_pedidos as $todos): ?>
                            <tr>
                                <td>
                                <?= $todos->pedido_id ?>
                                </td>
                                <td>
                                <?= $todos->producto_id ?>
                                </td>
                                <td>
                                <?= $todos->precio ?>
                                </td>
                                <td>
                                <?= $todos->cantidad ?>
                                </td>
                                <td>        
                                <?php if ( isset($id_empleado_buscar)): ?>
                                <?= $id_empleado_buscar ?>
                                <?php else: ?>
                                <p> </p>
                                <?php endif ?>
                                </td>

                            </tr>

            <?php endforeach; ?>
        </table>
        <a href="<?= site_url('/listaped') ?>" class="btn btn-warning ml-2">VER PEDIDOS</a>

    </body>
</html>
