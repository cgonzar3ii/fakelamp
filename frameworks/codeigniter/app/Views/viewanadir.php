<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>

<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        
        <title>Form añadir producto en categoria <?=$id_categoria_seleccionada?></title>
    </head>
    <body>


<h1>Form añadir producto en categoria <?=$id_categoria_seleccionada?></h1>
<div class="ml-5">
<form action="<?= site_url('/anadirproductos/submit/'.$id_categoria_seleccionada)?>" method="post">
            

            
            <label>ID producto</label><br>
            <input name="id" type="text" disabled value="generado automáticamente"><br><br>
            
            <label>Nombre Producto</label><br>
            <input name="nombreProd" type="text"><br><br>
            
            <label>ID Proveedor</label><br>
            <input name="proveedor_id" type="number" required><br><br>
            
            <label>ID Categoría</label><br>
            <input name="categoria_id" type="text" value="<?= $id_categoria_seleccionada ?>"><br><br>
            
            <label>Descripción (opcional)</label><br>
            <input name="descripcion" type="text"><br><br>
            
            <label>Precio</label><br>
            <input name="precio" type="number"><br><br>
            
            <label>Existencias</label><br>
            <input name="existencias" type="number"><br><br>

            <input type="submit"> 
            
        </form> 
    <br><br>
    <a href="<?= site_url('/listacat') ?>" class="btn btn-danger ml-2">VOLVER A CATEGORIAS</a>
</div>


    </body>
</html>
