<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of CarlosController
 *
 * @author carlos
 */
class CarlosController extends BaseController {

    public function listacategorias() {

        $datoscategoria = new \App\Models\NeptunoCategoria();

        $categorias ['resultadoselect'] = $datoscategoria->findAll();

        return view('viewcategorias', $categorias);
    }

    public function listaproductos($id_categoria_seleccionada) {

        $datosproducto = new \App\Models\NeptunoProducto();

        $array ['resultado_select_productos'] = $datosproducto->where('categoria_id', $id_categoria_seleccionada)->findAll();
        $array ['id_categoria_seleccionada'] = $id_categoria_seleccionada;

        return view('viewproductos', $array);
    }

    public function anadirproductos($id_categoria_seleccionada) {

        $array ['id_categoria_seleccionada'] = $id_categoria_seleccionada;
        return view('viewanadir', $array);
    }

    public function anadirproductossubmit($id_categoria_seleccionada) {

        //codigo insertar nuevo producto
        $datosproducto = new \App\Models\NeptunoProducto();

        $np_nombre = $this->request->getPost('nombreProd');
        $np_proveedor = $this->request->getPost('proveedor_id');
        $np_categoria = $this->request->getPost('categoria_id');
        $np_descripcion = $this->request->getPost('descripcion');
        $np_precio = $this->request->getPost('precio');
        $np_existencias = $this->request->getPost('existencias');

        $array_nuevo_prod = [
            "nombreProd" => $np_nombre,
            "proveedor_id" => $np_proveedor,
            "categoria_id" => $np_categoria,
            "descripcion" => $np_descripcion,
            "precio" => $np_precio,
            "existencias" => $np_existencias,
        ];

        $datosproducto->insert($array_nuevo_prod);

        ///////////
        // codigo para volver a mostrar productos del grupo seleccionado
        $datosproducto = new \App\Models\NeptunoProducto();

        $array ['resultado_select_productos'] = $datosproducto->where('categoria_id', $id_categoria_seleccionada)->findAll();
        $array ['id_categoria_seleccionada'] = $np_categoria;
        return view('viewproductos', $array);
    }

    public function listapedidos(){
        
        $datospedidos = new \App\Models\NeptunoPedidos();
        
        $array ['resultado_select_pedidos'] = $datospedidos->findAll();
        
        return view('viewpedidos', $array);
    }
    
    public function listapedidossearch(){
        
        $datospedidos = new \App\Models\NeptunoPedidos();
        $id_empleado_buscar = $this->request->getPost('id_empleado_buscar');        
        
        $array ['resultado_select_pedidos'] = $datospedidos
                                            ->select('detalle_venta.pedido_id, detalle_venta.producto_id, detalle_venta.precio, detalle_venta.cantidad')
                                            //->from('detalle_venta')
                                            ->join('pedidoventa','pedidoventa.id = detalle_venta.pedido_id','LEFT')
                                            ->where(['pedidoventa.empleado_id'=>$id_empleado_buscar])
                                            ->groupBy('detalle_venta.pedido_id')
                                            ->findAll();
        
        $array ['id_empleado_buscar'] = $id_empleado_buscar;
        
        return view('viewpedidos', $array);
    }

}
