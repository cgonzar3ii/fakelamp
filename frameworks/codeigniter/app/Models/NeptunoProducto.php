<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */


/**
 * Description of NeptunoProducto
 *
 * @author carlos
 */
namespace App\Models;
use CodeIgniter\Model;
class NeptunoProducto extends Model{
    
    protected $table = 'producto';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    
    protected $allowedFields = [
        'nombreProd',
        'proveedor_id',
        'categoria_id',
        'descripcion',
        'precio',
        'existencias',
    ];
    
}