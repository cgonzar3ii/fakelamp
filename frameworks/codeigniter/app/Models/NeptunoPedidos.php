<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;
/**
 * Description of NeptunoPedidos
 *
 * @author carlos
 */
class NeptunoPedidos extends Model{
    
    protected $table = 'detalle_venta';
    protected $primaryKey = 'pedido_id';
    protected $returnType = 'object';
    
}
