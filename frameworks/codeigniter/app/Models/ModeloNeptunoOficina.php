<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of ModeloNeptunoOficina
 *
 * @author carlos
 */
namespace App\Models;
use CodeIgniter\Model;
class ModeloNeptunoOficina {
    
    protected $table = 'oficina';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    
}
